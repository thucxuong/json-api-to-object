"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.arrayify = arrayify;
exports.createHashMap = createHashMap;
exports.LinkingRelationshipObject = LinkingRelationshipObject;
exports.default = resolveRelationships;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function arrayify() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

  return data ? Array.isArray(data) ? data : [data] : [];
}

function createHashMap(data, included) {
  if (!data) {
    return {};
  }
  var unifyArray = included ? [].concat(_toConsumableArray(arrayify(data)), _toConsumableArray(arrayify(included))) : arrayify(data);
  return unifyArray.length === 0 ? {} : unifyArray.reduce(function (obj, entity) {
    return _extends({}, obj, _defineProperty({}, entity.id, entity));
  }, {});
}

function LinkingRelationshipObject(listRel, hashMap) {
  return listRel.map(function (rel) {
    var resolvedRel = rel && rel.id && hashMap[rel.id] ? hashMap[rel.id] : null;
    if (resolvedRel) {
      Object.keys(rel).forEach(function (relKey) {
        if (!resolvedRel.relKey) {
          resolvedRel[relKey] = rel[relKey];
        }
      });
      return resolvedRel;
    }
    return rel;
  });
}

function resolveRelationships(data, included) {
  var hashMap = createHashMap(data, included);
  Object.keys(hashMap).forEach(function (id) {
    var entity = hashMap[id];
    if (!entity.relationships) {
      return;
    }
    entity.relationships = Object.keys(entity.relationships).reduce(function (rels, key) {
      var entityRel = entity.relationships[key];
      var listRel = arrayify(entityRel.data);
      var resolvedListRel = LinkingRelationshipObject(listRel, hashMap);
      return _extends({}, rels, _defineProperty({}, key, _extends({}, entityRel, {
        data: resolvedListRel.length > 1 ? resolvedListRel : resolvedListRel[0]
      })));
    }, {});
  });
}
