import { arrayify } from './src';

test('Arrayify: no params', () => {
  expect(arrayify()).toEqual([])
});

test('Arrayify: null params', () => {
  expect(arrayify(null)).toEqual([])
});

test('Arrayify: String params', () => {
  expect(arrayify('Test string')).toEqual(['Test string'])
});

test('Arrayify: Object params', () => {
  expect(arrayify({ test: 'Object' })).toEqual([{ test: 'Object' }])
});

test('Arrayify: Array params', () => {
  expect(arrayify([1,2])).toEqual([1,2])
});
