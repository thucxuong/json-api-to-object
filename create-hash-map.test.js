import { createHashMap } from './src';

test('createHashMap: return blank object if no params', () => {
  expect(createHashMap()).toEqual({})
});

test('createHashMap: return blank object if all params null', () => {
  expect(createHashMap(null, null)).toEqual({})
});

test('createHashMap: return blank object if data param null', () => {
  expect(createHashMap(null)).toEqual({})
});

test('createHashMap: return blank object if data param null and included param is (array | object)', () => {
  const includedIsArray = expect(createHashMap(null, [])).toEqual({});
  const includedIsObject = expect(createHashMap(null, {})).toEqual({});
  return includedIsArray && includedIsObject;
});

test('createHashMap: return data as value for an object with key is data.id while included is undefined', () => {
  expect(createHashMap({
    id: 'a3234',
    content: '2'
  })).toEqual({
    'a3234': {
      id: 'a3234',
      content: '2'
    }
  });
});

test('createHashMap: return hashed map from valid data and included params as objects', () => {
  expect(createHashMap({
    id: 'a3234',
    content: '1'
  }, {
    id: 'b23454',
    content: '2'
  })).toEqual({
    'a3234': {
      id: 'a3234',
      content: '1'
    },
    'b23454': {
      id: 'b23454',
      content: '2'
    }
  });
});

test('createHashMap: return hashed map from valid data and included params as arrays', () => {
  expect(createHashMap([{
    id: 'a3234',
    content: '1'
  }],[{
    id: 'b23454',
    content: '2'
  }])).toEqual({
    'a3234': {
      id: 'a3234',
      content: '1'
    },
    'b23454': {
      id: 'b23454',
      content: '2'
    }
  });
});
