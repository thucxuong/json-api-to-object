export function arrayify(data = []) {
  return data ? (Array.isArray(data) ? data : [data]) : [];
}

export function createHashMap(data, included) {
  if (!data) { return {}; }
  const unifyArray = included ? [...arrayify(data), ...arrayify(included) ] : arrayify(data);
  return unifyArray.length === 0 ? {} : unifyArray.reduce((obj, entity) => ({
      ...obj,
      [entity.id]: entity
    }), {});
}

export function LinkingRelationshipObject(listRel, hashMap) {
  return listRel.map(rel => {
    const resolvedRel = rel && rel.id && hashMap[rel.id] ? hashMap[rel.id] : null;
    if (resolvedRel) {
      Object.keys(rel).forEach(relKey => {
        if (!resolvedRel.relKey) {
          resolvedRel[relKey] = rel[relKey];
        }
      });
      return resolvedRel;
    }
    return rel;
  });
}

export default function resolveRelationships(data, included) {
  const hashMap = createHashMap(data, included);
  Object.keys(hashMap).forEach((id) => {
    const entity = hashMap[id];
    if (!entity.relationships) {
      return;
    }
    entity.relationships = Object.keys(entity.relationships).reduce((rels, key) => {
      const entityRel = entity.relationships[key];
      const listRel = arrayify(entityRel.data);
      const resolvedListRel = LinkingRelationshipObject(listRel, hashMap);
      return {
        ...rels,
        [key]: {
          ...entityRel,
          data: resolvedListRel.length > 1 ? resolvedListRel : resolvedListRel[0]
        },
      };
    }, {});
  });
}